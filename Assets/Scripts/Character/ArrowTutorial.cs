﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ArrowTutorial : MonoBehaviour
{
    public SpriteRenderer arrow;
    public float minDistanceToGoal;
    public bool dogMode;

    public Transform workBench;
    public Transform stairs;
    public Transform bed;

    private Vector3 pickedItemLocation;
    private Vector3 goal;

    [ReadOnly]
    public int step = 0;

    
    public void OnPickedItem()
    {
        pickedItemLocation = transform.position;
        arrow.enabled = true;
        goal = workBench.position;
    }

    public void HideArrow()
    {
        step = 0;
        arrow.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(!dogMode)
        {
            goal = stairs.position;
            arrow.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (arrow.enabled)
        {
            arrow.transform.up = goal - arrow.transform.position;
            if (Vector3.Distance(transform.position, goal) <= minDistanceToGoal)
            {
                if (dogMode)
                {
                    if (step == 0)
                    {
                        goal = pickedItemLocation;
                        step = 1;
                    }
                    else if (step == 1)
                    {
                        HideArrow();
                    }
                }
                else
                {
                    if (step == 0)
                    {
                        goal = bed.position;
                        step = 1;
                    }
                    else if (step == 1)
                    {
                        HideArrow();
                    }
                }
            }
        }
    }
}
