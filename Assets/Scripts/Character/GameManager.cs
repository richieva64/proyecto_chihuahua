﻿using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker.Actions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager singleton;

    public UnityEvent OnGameOver;

    public Image drunkMeter;
    public Image chaosMeter;

    public Image minuteHand;
    public float maxMinuteRotation;
    public Image hourHand;
    public float maxHourRotation;
    public Image clockFill;

    public float maxTime = 120;
    public float chaosPerItem = 0.1f;
    public float drunknessPerBeer = 0.2f;
    public float maxChaosLevelToWin = 0.8f;

    public DrunkVision drunkVision;
    public RepairMinigame repairMinigame;

    [System.NonSerialized]
    public float chaosLevel = 0;
    [System.NonSerialized]
    public float currentTime = 0;

    public float originalHourRotation;
    public float originalMinuteRotation;
    public Image gameOverImage;
    public Sprite looseSprite;
    public Sprite winSprite;

    public AudioClip winTheme;
    public AudioClip looseTheme;
    public AudioClip gameplayTheme;

    private Quaternion finalHourRotation;
    private Quaternion finalMinuteRotation;
    private AudioSource audioSource;
    bool gameOver = false;


    private void Awake()
    {
        if(singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    

    // Start is called before the first frame update
    void Start()
    {
        // if (drunkVision != null)
        // {
        //     drunkVision.OnDrunknessChanged?.AddListener(OnDrunknessChanged);
        //     OnDrunknessChanged(drunkVision.DrunkLevel);
        // }
        chaosMeter.fillAmount = chaosLevel;
        finalHourRotation = Quaternion.Euler(0, 0, maxHourRotation);
        finalMinuteRotation = Quaternion.Euler(0, 0, maxMinuteRotation);
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = gameplayTheme;
        audioSource.Play();
    }

    public void OnDrunknessChanged(float drunknessLevel)
    {
        drunkMeter.fillAmount = drunknessLevel;
    }

    public void OnItemDestroyed()
    {
        chaosLevel += chaosPerItem;
        chaosMeter.fillAmount = chaosLevel;
    }

    public void OnItemRepaired()
    {
        chaosLevel -= chaosPerItem;
        chaosMeter.fillAmount = chaosLevel;
    }

    public void OnPlayerReachedBed()
    {
        GameOver();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
            
        if (gameOver) return;

        currentTime += Time.deltaTime;

        hourHand.rectTransform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(originalHourRotation, maxHourRotation, currentTime / maxTime));
        minuteHand.rectTransform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(originalMinuteRotation, maxMinuteRotation, currentTime / maxTime));
        clockFill.fillAmount = currentTime / maxTime;

        if (currentTime >= maxTime)
        {
            GameOver();
            gameOver = true;
        }

        OnDrunknessChanged(drunkVision.DrunkLevel);

    }

    private void GameOver()
    {
        OnGameOver.Invoke();
        
        if(currentTime >= maxTime || chaosLevel >= maxChaosLevelToWin)
        {
            //Pierde juego
            gameOverImage.sprite = looseSprite;
            audioSource.Stop();
            audioSource.clip = looseTheme;
            audioSource.Play();
        }
        else
        {
            //Gana juego
            gameOverImage.sprite = winSprite;
            audioSource.Stop();
            audioSource.clip = winTheme;
            audioSource.Play();
        }

        gameOverImage.gameObject.SetActive(true);
    }
    

    private void OnDestroy()
    {
        if(drunkVision != null) drunkVision.OnDrunknessChanged?.RemoveListener(OnDrunknessChanged);
    }
}
