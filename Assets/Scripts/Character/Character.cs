﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public int numPlayer = 1;
    public float speed;

    public Animator animator;
    public void Move(Vector2 direction)
    {
        transform.Translate(direction * Time.deltaTime * speed, Space.World);
        if (animator != null)
        {
            animator.SetFloat("VelX",direction.x);
            animator.SetFloat("VelY",direction.y);
        }
    }

    private void Update()
    {
        // if (Input.GetKey(KeyCode.W))
        // {
        //     Move(Vector2.up);
        // }
        //
        // if (Input.GetKey(KeyCode.S))
        // {
        //     Move(Vector2.down);
        // }
        //
        // if (Input.GetKey(KeyCode.A))
        // {
        //     Move(Vector2.left);
        // }
        //
        // if (Input.GetKey(KeyCode.D))
        // {
        //     Move(Vector2.right);
        // }
    }

    
}
