﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDrunk : MonoBehaviour, IControllable
{
    public int numPlayer = 2;
    public float speed;
    public Animator animator;
    public DrunkVision drunkVision;
    public float rayCastDistance;
    public LayerMask rayCastLayer;
    public bool enableInput = true;
    public bool colliding;
    public void Move(Vector2 direction)
    {
        if (!enableInput) return;

        Debug.DrawRay(transform.position, rayCastDistance*direction,Color.red);
        colliding = true;

        if (Physics.Raycast(transform.position, direction,rayCastDistance,rayCastLayer))
        //if (Physics.CheckSphere(transform.position, rayCastDistance, rayCastLayer))
            return;
        
        colliding = false;
        transform.Translate(direction * Time.deltaTime * speed, Space.World);
        if (animator != null)
        {
            animator.SetFloat("VelX", direction.x);
            animator.SetFloat("VelY", direction.y);
        }
    }

    public void ActionButtonPressed()
    {
        //throw new System.NotImplementedException();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<IDestructable>() != null)
        {
            collision.GetComponent<IDestructable>().Destroy();
            GameManager.singleton.OnItemDestroyed();
        }

        if (collision.tag == "Consumable")
        {
            drunkVision.DrunkLevel+= collision.GetComponent<Consumable>().PickUpConsumable();
        }

        if (collision.tag == "BedFinish")
        {
            enableInput = false;
            GameManager.singleton.OnPlayerReachedBed();
        }
    }
}
