﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDog : MonoBehaviour, IControllable
{
    public int numPlayer = 1;
    public float speed;
    public Animator animator;
    public ArrowTutorial arrowTutorial;

    ICollectable theCollisionCollectable;
    public CollectableData collectedFurniture = new CollectableData();
    public bool atWorkStation = false;
    public GameObject objectOnHold;
    public bool enableInput = true;

    public float moveHammerCooldown = 0.1f;
    private float moveHammerTimeCounter = 0f;

    public float rayCastDistance;
    public LayerMask rayCastLayer;
    public bool colliding;
    private void Start()
    {
        RepairMinigame.Instance.OnItemRepaired += MinigameFinished;
    }


    public void Move(Vector2 direction)
    {
        if (!enableInput) return;


        if (atWorkStation)
        {
            if (moveHammerTimeCounter <= 0)
            {
                if (direction.y > 0)
                    RepairMinigame.Instance.MoveHammer(1);
                else
                if (direction.y < 0)
                    RepairMinigame.Instance.MoveHammer(-1);
                moveHammerTimeCounter = moveHammerCooldown;
            }
            
        }
        else
        {
            Debug.DrawRay(transform.position, rayCastDistance*direction,Color.red);
            colliding = true;

            if (Physics.Raycast(transform.position, direction,rayCastDistance,rayCastLayer))
                //if (Physics.CheckSphere(transform.position, rayCastDistance, rayCastLayer))
                return;
        
            colliding = false;
            
            transform.Translate(direction * Time.deltaTime * speed, Space.World);
            if (animator != null)
            {
                animator.SetFloat("VelX", direction.x);
                animator.SetFloat("VelY", direction.y);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        theCollisionCollectable = collision.GetComponent<ICollectable>();
        if (theCollisionCollectable != null && collectedFurniture.numPieces == 0)
        {
            theCollisionCollectable.Pickup();
            if(collision.tag == "Rubbish")
            {
                arrowTutorial.OnPickedItem();
                collectedFurniture = theCollisionCollectable.GetData();
                HoldSomething(true, true);
            }
        }

        if (!atWorkStation && collectedFurniture.numPieces > 0 && collision.tag == "WorkStation" && !collectedFurniture.repaired)
        {
            OpenWorkStation();
        }

        //Dropping and clearing collectedFurniture
        if (collision.tag == "ObjectOrigin" && collectedFurniture.numPieces > 0  && collectedFurniture.repaired)
        {
            DropoutZone zone = collision.GetComponent<DropoutZone>();
            if (zone != null)
            {
                HoldSomething(false);
                zone.DropItem();
                GameManager.singleton.OnItemRepaired();
            }
            collectedFurniture = new CollectableData();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "WorkStation")
        {
            atWorkStation = false;
        }
    }

    public void OpenWorkStation()
    {
        atWorkStation = true;
        RepairMinigame.Instance.StartRepair(collectedFurniture.numPieces, collectedFurniture.sprite);
    }

    public void ActionButtonPressed()
    {
        if(atWorkStation)
            RepairMinigame.Instance.MakeRepair();

    }

    public void MinigameFinished()
    {
        atWorkStation = false;
        collectedFurniture.repaired = true;
        HoldSomething(true, false);

    }

    private void Update()
    {
        if (moveHammerTimeCounter > 0)
            moveHammerTimeCounter -= Time.deltaTime;
    }

    public void HoldSomething(bool doHold, bool holdTrashItem = true)
    {
        if (doHold)
        {
            animator.Play("DogAnimatorHolding");
            objectOnHold.SetActive(true);
            if(holdTrashItem)
                objectOnHold.GetComponent<SpriteRenderer>().sprite = collectedFurniture.trashSprite;
            else
                objectOnHold.GetComponent<SpriteRenderer>().sprite = collectedFurniture.sprite;
        }
        else
        {
            animator.Play("DogAnimator");
            objectOnHold.SetActive(false);

        }
    }

    public void DisableInput()
    {
        enableInput = false;
    }
}
