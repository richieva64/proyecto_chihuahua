﻿using UnityEngine;

public interface IControllable
{
    void Move(Vector2 direction);

    void ActionButtonPressed();
}
