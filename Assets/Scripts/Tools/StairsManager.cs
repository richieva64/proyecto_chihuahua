﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsManager : MonoBehaviour
{
    public Transform teleportPoint;
    public int drunkLayer;
    public int dogLayers;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == drunkLayer || other.gameObject.layer == dogLayers)
            other.gameObject.transform.position = teleportPoint.position;
    }
}
