﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DogInput : MonoBehaviour
{
    
    public Vector2 movement;
    private IControllable character;
    public float actionButtonCooldown = 0.3f;
    public float actionButtonTimeCounter = 0f;
    

    private void Start()
    {
        character = gameObject.GetComponent<IControllable>();
    }
    private void Update()
    {
        
        if (character != null)
        {
            character.Move(movement);
        }

        if (actionButtonTimeCounter > 0)
            actionButtonTimeCounter -= Time.deltaTime;
    }
    
    public void MovementDog(InputAction.CallbackContext context)
    {
        movement = context.ReadValue<Vector2>();
    }

    public void ActionButtonDown(InputAction.CallbackContext context)
    {
        if (actionButtonTimeCounter <= 0)
        {
            character.ActionButtonPressed();
            actionButtonTimeCounter = actionButtonCooldown;
        }
    }
}
