﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class DrunkVision : MonoBehaviour
{
    public UnityEvent<float> OnDrunknessChanged;

    [PropertyRange(0, 1)]
    [ShowInInspector]
    private float drunkLevel;
    
    public float drunkIncrement;

    public float DrunkLevel
    {
        get
        {
            return drunkLevel;
        }
        set
        {
            drunkLevel = value;
            OnDrunknessChanged?.Invoke(drunkLevel);
        }
    }
    public _2dxFX_Liquid drunkVision;
    public MeshRenderer distortion;

    public float minVision, maxVision;
    public float minDistortion, maxDistortion;
    public Color minColorDistortion, maxColorDistortion;
    void Start()
    {
        
    }

    public void DrinkBeer()
    {
        DrunkLevel += GameManager.singleton.drunknessPerBeer;
    }

    void Update()
    {
        drunkVision._Alpha = Mathf.Lerp(minVision, maxVision, DrunkLevel);
        distortion.material.SetFloat("_Distortion",Mathf.Lerp(minDistortion,maxDistortion,DrunkLevel));
        distortion.material.SetColor("_PortalColor", Color.Lerp(minColorDistortion,maxColorDistortion, DrunkLevel));

        DrunkLevel += drunkIncrement *Time.deltaTime;
        DrunkLevel = Mathf.Clamp01(DrunkLevel);
    }
}
