﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class DrunkInput : MonoBehaviour
{
    [PropertyRange(0,1)]
    public float drunkLevel;
    public float drunkPower;

    public Vector2 movement;
    public Vector2 modifyMovement;
    public Vector2 influence;
    
    [ProgressBar(0,1)]
    public float factor;
    public float decayFactor;
    
    public float minTime;
    public float maxTime;
    
    
    public Text drunkInputText;
    public Text drunkModifyText;

    private IControllable character;

    public DrunkVision drunkVision;
    private void Start()
    {
        character = gameObject.GetComponent<IControllable>();

        StartCoroutine(RandomModify(3));
    }

    public IEnumerator RandomModify(float timeRepeat)
    {
        modifyMovement = drunkPower*drunkLevel*Random.insideUnitCircle;
        factor = 1;
        yield return new WaitForSeconds(timeRepeat);
        StartCoroutine(RandomModify(Random.Range(minTime, maxTime)));
    }
    private void Update()
    {
        factor *= decayFactor;
        influence = modifyMovement * factor;;
        
        if(character != null)
            character.Move(movement + influence);
        drunkLevel = drunkVision.DrunkLevel;
    }

    public void MovementDrunk(InputAction.CallbackContext context)
    {
        movement = context.ReadValue<Vector2>();
        //modifyMovement = movement + drunkPower*drunkLevel*Random.insideUnitCircle;

        if(drunkInputText != null)
            drunkInputText.text = "" + movement;
        if(drunkModifyText != null)
            drunkModifyText.text = "" + modifyMovement;
    }
}
