﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class InputTestGUI : MonoBehaviour
{
    public Text drunkInputText;
    public Text dogInputText;
    void Start()
    {
        
    }

    public void MovementDrunk(InputAction.CallbackContext context)
    {
        if(drunkInputText != null)
            drunkInputText.text = "" + context.ReadValue<Vector2>();
    }
    public void MovementDog(InputAction.CallbackContext context)
    {
        if(dogInputText != null)
            dogInputText.text = "" + context.ReadValue<Vector2>();
    }

    
}
