﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class RepairMinigame : MonoBehaviour
{
    public static RepairMinigame instance;
    public static RepairMinigame Instance
    {
        get
        {
            if (instance != null) return instance;
            else
            {
                instance = FindObjectOfType<RepairMinigame>();
                return instance;
            }
        }
    }

    public bool testMode = false;

    public RectTransform line;
    public RectTransform parentRect;
    public float movementSpeed = 0.1f;
    public int numberOfBrokenParts = 2;
    public RectTransform hammerIcon;
    public RepairHammer hammer;
    public Image itemImage;

    public RectTransform maskPrefab;
    public RectTransform maskGridContainer;
    public int gridSizeX = 3;
    public int gridSizeY = 3;

    public AudioClip successSound;
    public AudioClip errorSound;
    public AudioClip winSound;

    /// <summary>
    /// Se dispara este evento al terminar el minijuego exitosamente
    /// </summary>
    public UnityAction OnItemRepaired;

    private float startingPosition;
    private float endingPosition; 
    private float lineLerpTime = 0;
    private int hammerPosition = 0;
    private List<RectTransform> brokenTiles = new List<RectTransform>();
    private bool repairing = false;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = -hammerIcon.rect.width;
        endingPosition = parentRect.rect.width + hammerIcon.rect.width;
        audioSource = GetComponent<AudioSource>();
        if(testMode) StartRepair(3, itemImage.sprite);
    }

    /// <summary>
    /// Inicia minijuego de reparacion
    /// </summary>
    /// <param name="numberOfBrokenParts"></param>
    /// <param name="itemSprite"></param>
    public void StartRepair(int numberOfBrokenParts, Sprite itemSprite)
    {
        hammerPosition = Random.Range(0, gridSizeY);
        parentRect.gameObject.SetActive(true);
        itemImage.sprite = itemSprite;
        this.numberOfBrokenParts = numberOfBrokenParts;
        SetHammerPosition();
        CreateMaskGrid(numberOfBrokenParts);
        repairing = true;
    }

    
    // Update is called once per frame
    void Update()
    {
        if (!repairing) return;

        MoveLineUpdate();

        if (testMode) TestInput();
    }

    private void TestInput()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveHammer(-1);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveHammer(1);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MakeRepair();
        }
    }

    private void MoveLineUpdate()
    {
        lineLerpTime += Time.deltaTime * movementSpeed;
        if (lineLerpTime > 1) lineLerpTime = 0;

        float xPos = Mathf.Lerp(startingPosition, endingPosition, lineLerpTime);
        line.anchoredPosition = new Vector2(xPos, line.anchoredPosition.y);
    }

    private void CreateMaskGrid(int numberOfBrokenTiles)
    {
        List<Vector2> brokenTilePositions = new List<Vector2>();
        
        for(int i = 0; i < numberOfBrokenTiles; i++)
        {
            bool success = false;
            while(!success)
            {
                Vector2 randomTile = new Vector2(Random.Range(0, gridSizeX), Random.Range(0, gridSizeY));
                if (!brokenTilePositions.Contains(randomTile))
                {
                    brokenTilePositions.Add(randomTile);
                    success = true;
                }
            }
        }

        foreach(Vector2 tile in brokenTilePositions)
        {
            RectTransform maskRect = Instantiate(maskPrefab, maskGridContainer);
            maskRect.anchoredPosition = new Vector2(tile.x * maskRect.rect.width, tile.y * -maskRect.rect.height);
            brokenTiles.Add(maskRect);
        }
    }

    private void SetHammerPosition()
    {
        hammerIcon.anchoredPosition = new Vector2(hammerIcon.anchoredPosition.x, (hammerPosition * -maskPrefab.rect.height) - maskPrefab.rect.height/2 - hammerIcon.rect.height/2);
    }

    /// <summary>
    /// Mueve el martillo de reparacion hacia arriba y hacia abajo, pasar 1 para arriba y -1 para abajo
    /// </summary>
    /// <param name="direction"></param>
    public void MoveHammer(int direction)
    {
        hammerPosition = Mathf.Clamp(hammerPosition - direction, 0, gridSizeY-1);
        SetHammerPosition();
    }

    /// <summary>
    /// Activa el martillo para intentar reparar en el tile que esta tocando
    /// </summary>
    public void MakeRepair()
    {
        RectTransform maskRect = hammer.CheckCollision();
        if(maskRect != null)
        {
            audioSource.PlayOneShot(successSound);
            brokenTiles.Remove(maskRect);
            Destroy(maskRect.gameObject);
            if(brokenTiles.Count <= 0)
            {
                audioSource.PlayOneShot(winSound);
                repairing = false;
                OnItemRepaired?.Invoke();
                parentRect.gameObject.SetActive(false);
            }
        }
        else
        {
            audioSource.PlayOneShot(errorSound);
            for(int i = brokenTiles.Count-1; i >= 0; i--)
            {
                Destroy(brokenTiles[i].gameObject);
            }
            brokenTiles.Clear();
            CreateMaskGrid(numberOfBrokenParts);
            lineLerpTime = 0;
        }
    }
}
