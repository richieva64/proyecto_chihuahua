﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairHammer : MonoBehaviour
{
    private GameObject otherCollider;

    public RectTransform CheckCollision()
    {
        if(otherCollider != null)
        {
            return otherCollider.GetComponent<RectTransform>();
        }
        else
        {
            return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        otherCollider = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        otherCollider = null;
    }
}
