﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public List<Sprite> possibleSprites = new List<Sprite>();
    public CollectableData collectableData;
    public GameObject normalStateObject;
    public GameObject destroyedStateObject;
    public GameObject siluheteStateObject;

    public List<SpriteRenderer> spriteRenderersForNormalSprite = new List<SpriteRenderer>();
    public List<SpriteRenderer> spriteRenderersForBrokenSprite = new List<SpriteRenderer>();
    private AudioSource audioSource;
    public List<AudioClip> destructionSounds = new List<AudioClip>();
    public List<AudioClip> pickUpSounds = new List<AudioClip>();
    public List<AudioClip> settingNormalSounds = new List<AudioClip>();

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        if (possibleSprites.Count > 0)
            collectableData.sprite = possibleSprites[Random.Range(0, possibleSprites.Count)];

        foreach(SpriteRenderer spriterend in spriteRenderersForNormalSprite)
        {
            spriterend.sprite = collectableData.sprite;
        }
        foreach (SpriteRenderer spriterend in spriteRenderersForBrokenSprite)
        {
            spriterend.sprite = collectableData.trashSprite;
        }
    }

    private void Start()
    {
        collectableData.repaired = true;
        normalStateObject.SetActive(true);
        destroyedStateObject.SetActive(false);
        siluheteStateObject.SetActive(false);
    } 

    public void SetNormalState()
    {
        audioSource.PlayOneShot(settingNormalSounds[Random.Range(0, settingNormalSounds.Count)]);

        collectableData.repaired = true;
        normalStateObject.SetActive(true);
        destroyedStateObject.SetActive(false);
        siluheteStateObject.SetActive(false);
    }

    public void DestroyPiece()
    {
        audioSource.PlayOneShot(destructionSounds[Random.Range(0, destructionSounds.Count)]);

        collectableData.repaired = false;
        normalStateObject.SetActive(false);
        destroyedStateObject.SetActive(true);
    }

    public void PickupPiece()
    {
        audioSource.PlayOneShot(pickUpSounds[Random.Range(0, pickUpSounds.Count)]);

        destroyedStateObject.SetActive(false);
        siluheteStateObject.SetActive(true);
    }

}
