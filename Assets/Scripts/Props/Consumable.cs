﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : MonoBehaviour
{
    public float valueAdded;
    public GameObject particleObject;
    public AudioSource audioSource;
    public List<AudioClip> sounds = new List<AudioClip>();


    public float PickUpConsumable()
    {
        audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Count)]);
        particleObject.SetActive(true);
        StartCoroutine(Dissappear());
        return valueAdded;
    }

    public IEnumerator Dissappear()
    {
        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);
    }
}
