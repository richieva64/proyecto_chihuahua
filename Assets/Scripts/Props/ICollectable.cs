﻿public interface ICollectable
{
    void Pickup();
    CollectableData GetData();
}
