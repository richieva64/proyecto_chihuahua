﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour, ICollectable
{
    public InteractableObject interactableObject;

    public void Pickup()
    {
        interactableObject.PickupPiece();
    }
    public CollectableData GetData()
    {
        return interactableObject.collectableData;
    }
}

[System.Serializable]
public class CollectableData{
    public Sprite sprite;
    public Sprite trashSprite;
    public int numPieces;
    public bool repaired = true;
}

