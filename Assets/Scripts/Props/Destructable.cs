﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour, IDestructable
{
    public InteractableObject interactableObject;

    public void Destroy()
    {
        interactableObject.DestroyPiece();
    }
}
