﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropoutZone : MonoBehaviour
{
    public InteractableObject interactableObject;

    public void DropItem()
    {
        interactableObject.SetNormalState();
    }
}
